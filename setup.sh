#!/bin/bash

yum -y localinstall /usr/local/src/mysql57-community-release-el7-8.noarch.rpm
yum -y install epel-release
yum -y install gcc \
			   boost-devel \
               gcc-c++ \
               gperf \
               libevent-devel \
               libuuid-devel \
               mysql-community-devel \
               make \
               supervisor

cd /usr/local/src
curl -k -LO https://launchpad.net/gearmand/1.2/1.1.12/+download/gearmand-1.1.12.tar.gz
tar xzf gearmand-1.1.12.tar.gz
cd gearmand-1.1.12
./configure
make
make install
mkdir -p /usr/local/var/log

cat <<EOF > /etc/supervisord.d/gearmand.ini
[supervisord]
nodaemon=true

[program:gearmand]
command=/usr/local/sbin/gearmand -d 
autostart=true
autorestart=true
EOF
